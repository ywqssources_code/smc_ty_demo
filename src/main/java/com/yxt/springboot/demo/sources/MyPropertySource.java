package com.yxt.springboot.demo.sources;

import org.springframework.core.env.PropertySource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author smc
 * @date 2019-01-10 18:49
 * @since
 **/
public class MyPropertySource extends PropertySource<Map<String,Object>> {


    public MyPropertySource(String name, Map<String, Object> source) {
        super(name, source);
    }

    @Override
    public Object getProperty(String name) {
        return this.getSource().get(name);
    }
}
