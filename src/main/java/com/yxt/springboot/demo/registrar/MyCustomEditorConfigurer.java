package com.yxt.springboot.demo.registrar;

import io.micrometer.core.lang.Nullable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditor;
import java.util.Map;

/**
 * @author smc
 * @date 2019-01-11 17:21
 * @since
 **/
@Configuration
public class MyCustomEditorConfigurer implements BeanFactoryPostProcessor {

    protected final Log logger = LogFactory.getLog(getClass());

    @Nullable
    private PropertyEditorRegistrar[] propertyEditorRegistrars;

    @Nullable
    private Map<Class<?>, Class<? extends PropertyEditor>> customEditors;

    public  MyCustomEditorConfigurer(PropertyEditorRegistrar[] propertyEditorRegistrars){
        this.propertyEditorRegistrars = propertyEditorRegistrars;
    }

    public  MyCustomEditorConfigurer(){
        System.out.println("xxxxxxx");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        if (this.propertyEditorRegistrars != null) {
            for (PropertyEditorRegistrar propertyEditorRegistrar : this.propertyEditorRegistrars) {
                beanFactory.addPropertyEditorRegistrar(propertyEditorRegistrar);
            }
        }

        if (this.customEditors != null) {
            this.customEditors.forEach(beanFactory::registerCustomEditor);
        }
    }
}
