package com.yxt.springboot.demo.registrar;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.Configuration;

import java.beans.PropertyEditor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author smc
 * @date 2019-01-11 16:42
 * @since
 **/
@Configuration
public class MyPropertyEditorRegistrar implements PropertyEditorRegistrar {


    @Override
    public void registerCustomEditors(PropertyEditorRegistry registry) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        registry.registerCustomEditor(Date.class,new CustomDateEditor(sdf,true));
    }


}
