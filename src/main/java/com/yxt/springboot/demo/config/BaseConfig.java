package com.yxt.springboot.demo.config;

import com.yxt.springboot.demo.registrar.MyPropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author smc
 * @date 2019-01-11 17:45
 * @since
 **/
@Configuration
public class BaseConfig {


    @Bean
    public CustomEditorConfigurer createCustomEditorConfigurer(PropertyEditorRegistrar[] propertyEditorRegistrarList){
        System.out.println("========");
        CustomEditorConfigurer customEditorConfigurer = new CustomEditorConfigurer();
        customEditorConfigurer.setPropertyEditorRegistrars(propertyEditorRegistrarList);
        return customEditorConfigurer;
    }
}
