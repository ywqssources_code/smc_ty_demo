package com.yxt.springboot.demo.file;

import com.yxt.springboot.demo.sources.MyPropertySource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.HashMap;
import java.util.Map;

/**
 * @author smc
 * @date 2019-01-10 16:18
 * @since
 **/
public class MyEnvironmentPostProcessor implements EnvironmentPostProcessor {

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        String port = environment.getProperty("server.port");
        System.out.printf("端口号: %s \n",port);
        Map<String,Object> map = new HashMap<>();
        map.put("user.name","孙明春");
        map.put("user.age",1);
        MyPropertySource myPropertySource = new MyPropertySource("myprop",map);
        environment.getPropertySources().addLast(myPropertySource);
        System.out.println("==================");





    }
}
