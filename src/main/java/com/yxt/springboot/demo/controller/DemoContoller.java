package com.yxt.springboot.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author smc
 * @date 2018-12-27 18:34
 * @since
 **/
@RestController
@RequestMapping("demo")
public class DemoContoller {

    @Value("${user.birthday}")
    private Date birthday;

    @Value("${test.name}")
    private String test;

    @Value("${random.int}")
    private int randomInt;

    @Value("${user.name}")
    private String username;

    @Value("${user.age}")
    private int age;


    @RequestMapping("helloworld")
    public String helloWorld(){
        return "hello_world: "+ randomInt+" ,username: "+ username+" ,age: "+ age+" ,test: "+ test ;
    }
}


