package com.yxt.springboot.demo.env;

import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author smc
 * @date 2019-01-10 08:42
 * @since
 **/
public class SysAndProDemo {

    public static void main(String[] args) {

       Map<String,Object>  emvMap = (Map)System.getenv();

       Iterator  iterator = emvMap.entrySet().iterator();
       while (iterator.hasNext()){
          Map.Entry<String, Object> env = (Map.Entry<String, Object>) iterator.next();
           System.out.printf("%s=%s \n",env.getKey(),env.getValue());

       }

//        Stream.of(emvMap.entrySet().iterator()).forEach(e->{
//                  e.
//        });

        System.out.println("====================end===================");

        Map<String ,Object> proMap =(Map) System.getProperties();

        Iterator  ii = proMap.entrySet().iterator();
        while (ii.hasNext()){
            Map.Entry<String, Object> env = (Map.Entry<String, Object>) ii.next();
            System.out.printf("%s=%s \n",env.getKey(),env.getValue());

        }



//        Thread.currentThread().getContextClassLoader().loadClass()
    }
}
